---
title: Recovery Codes
metaTitle: Recovery Codes | Psono Documentation
meta:
  - name: description
    content: How to setup and use a recovery code
---

# Recovery Codes

## Preamble

It is essential to be able to recovery your account if you ever forget your master password. You can create so called "recovery codes",
that will allow you to recover your account in such an event. Without a recovery account it is cryptographically impossible
to gain access, so normal "password resets" like you might know from other websites do not work with Psono. This is due
to the client side encryption.

## Setup Recovery Code

We assume that you can still login with a Psono client, so you have not yet lost access. It does not matter if it's the web client or a browser extension.
If you already have lost access, then please check if you are maybe still logged in somewhere else on another device.

1)  Login into your account:

![Step 1 Login](/images/user/recovery_codes/step1-login.jpg)

2)  Go to "Account":

![Step 2 go to account](/images/user/recovery_codes/step2-go-to-account.jpg)

3)  Select the “Generate Password Recovery” tab:

![Step 3 Select generate password recovery](/images/user/recovery_codes/step3-select-generate-password-recovery-tab.jpg)

4)  Click generate:

![Step 4 Click Generate](/images/user/recovery_codes/step4-click-generate.jpg)

5)  Store the recovery information

You will see now an output like shown below. Store the "Code" and / or the words (e.g. follow senior ... gown) savely offline.
If you forget your masterpassword you will need this information.

![Step 5 Store recovery information](/images/user/recovery_codes/step5-store-recovery-information.jpg)

::: warning
Any recovery code that you might have created before has been invalidated, so only the newest recovery code will work!
:::


## Regain access to an account with a Recovery Code

In the event that you might have forgotten your masterpassword now, you can use the recovery code to regain access.

1)  Go to "Lost Password?":

![Step 1 Go to "Lost Password"](/images/user/recovery_codes/step10-go-to-lost-password.jpg)

2)  Enter recovery information:

You can enter now either the code:

![Step 2a Enter Code](/images/user/recovery_codes/step11-enter-code.jpg)

or as an alternative you can enter the word list:

![Step 2b Enter Word List](/images/user/recovery_codes/step12-enter-word-list.jpg)

3)  Enter new password

You can enter now a new password

![Step 3 Enter New Password](/images/user/recovery_codes/step13-enter-new-password.jpg)

and confirm everything by clicking "Set new password".

You can now use the new password to login again.
