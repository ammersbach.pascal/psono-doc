---
title: Audit Log
metaTitle: Audit Log | Psono Documentation
meta:
  - name: description
    content: Configuration of Audit Logging
---

# Audit Log

## Preamble

This guide explains how to configure audit logging. All interactions of clients with the server produce a corresponding
log entry. Each log entry will contain the ip, the resource accessed, whether it was successful or not, the hostname that
was accessed and a more or less descriptive description of the event.

An example of such a log can be seen here:

```
2020-05-24T07:55:37.720973 logger=restapi.views.info, user_ip=10.10.10.145, req_method=GET, req_url=/info/, success=True, hostname=0a5048185933, status=HTTP_200_OK, event=READ_INFO_SUCCESS
2020-05-24T07:55:40.190189 logger=restapi.views.login, user_ip=10.10.10.145, req_method=POST, req_url=/authentication/login/, success=True, hostname=0a5048185933, status=HTTP_200_OK, event=LOGIN_STARTED_SUCCESS, user=demo2@example.com, user_id=ba504938-9a23-4162-867f-6840c301bc20
2020-05-24T07:55:40.927078 logger=restapi.views.activate_token, user_ip=10.10.10.145, req_method=POST, req_url=/authentication/activate-token/, success=True, hostname=0a5048185933, status=HTTP_200_OK, event=LOGIN_ACTIVATE_TOKEN_SUCCESS, user=demo2@example.com, user_id=ba504938-9a23-4162-867f-6840c301bc20, kwarg_token_id=a1398962-05a7-433d-a8f3-999df3e157b8
2020-05-24T07:55:41.027323 logger=restapi.views.status, user_ip=10.10.10.145, req_method=GET, req_url=/user/status/, success=True, hostname=0a5048185933, status=HTTP_200_OK, event=READ_STATUS_SUCCESS, user=demo2@example.com, user_id=ba504938-9a23-4162-867f-6840c301bc20
2020-05-24T07:55:41.416086 logger=restapi.views.datastore, user_ip=10.10.10.145, req_method=GET, req_url=/datastore/, success=True, hostname=0a5048185933, status=HTTP_200_OK, event=LIST_ALL_DATASTORES_SUCCESS, user=demo2@example.com, user_id=ba504938-9a23-4162-867f-6840c301bc20
2020-05-24T07:55:41.473771 logger=restapi.views.datastore, user_ip=10.10.10.145, req_method=GET, req_url=/datastore/bed4572d-6a28-4c00-94c2-dd45322de1d0/, success=True, hostname=0a5048185933, status=HTTP_200_OK, event=LIST_DATASTORE_SUCCESS, user=demo2@example.com, user_id=ba504938-9a23-4162-867f-6840c301bc20, kwarg_datastore_id=bed4572d-6a28-4c00-94c2-dd45322de1d0
2020-05-24T07:55:41.936556 logger=restapi.views.share_right, user_ip=10.10.10.145, req_method=GET, req_url=/share/right/, success=True, hostname=0a5048185933, status=HTTP_200_OK, event=READ_GROUP_SHARE_RIGHTS_SUCCESS, user=demo2@example.com, user_id=ba504938-9a23-4162-867f-6840c301bc20
2020-05-24T07:55:42.002075 logger=restapi.views.share, user_ip=10.10.10.145, req_method=GET, req_url=/share/de10070a-e836-4fb7-838c-89a85486c7c5/, success=True, hostname=0a5048185933, status=HTTP_200_OK, event=READ_SHARE_SUCCESS, user=demo2@example.com, user_id=ba504938-9a23-4162-867f-6840c301bc20, kwarg_share_id=de10070a-e836-4fb7-838c-89a85486c7c5
2020-05-24T07:57:11.970816 logger=restapi.views.health_check, user_ip=127.0.0.1, req_method=GET, req_url=/healthcheck/, success=True, hostname=0a5048185933, status=HTTP_200_OK, event=CHECK_HEALTH_SUCCESS
```

These logs should be sent to your logging aggregator with appropriate retention periods and access control.
All logging solutions (ELK, Splunk, ...) are supported and can be  used visualize the log nicely.

::: tip
This feature is only available in the Enterprise Edition.
:::

## Server (settings.yaml)

1.  Create logging folder

    Psono will store the `audit.log` in this logging folder.

	```bash
	mkdir -p /var/log/psono_ee
	```

2.  Adjust docker run command

    You were starting the Psono server with this command:

	```bash
    docker run --name psono-server-enterprise \
        --sysctl net.core.somaxconn=65535 \
        -v /path/to/modified/settings.yaml:/root/.psono_server/settings.yaml \
        -v /path/to/log/folder:/var/log/psono \
        -d --restart=unless-stopped -p 10100:80 psono/psono-server-enterprise:latest
	```

	Adjust this to include:

	```bash
    ...
        -v /var/log/psono_ee:/var/log/psono \
    ...
	```
    Restart the server afterwards


3.  Adjust the setting.yml as shown below

    During the installation of the server you have created a settings.yaml that needs to be adjusted now.

	```yaml
	LOGGING_AUDIT: True
	```

    Restart the server afterwards

## Other Configuration

### Whitelisting events

By default Psono will log all events. If you discover that you only want to have specific events you can filter them
with the following entry in your `settings.yml`:

```yaml
LOGGING_AUDIT_WHITELIST: ['FIRST_EVENT_CODE', 'SECOND_EVENT_CODE']
```


### Blacklisting events

An alternative to whitelisting events is blacklisting events. This can be useful if you want to exclude some events.
You can achieve this with the following entry in your `settings.yml`:

```yaml
LOGGING_AUDIT_BLACKLIST: ['FIRST_EVENT_CODE', 'SECOND_EVENT_CODE']
```

### Splunk

If you are using Splunk, then you can use our App and Add-On here, which provides the necessary log format parser and
preconfigured dashboards.

- [Splunk App for Psono](https://splunkbase.splunk.com/app/4455/)
- [Splunk Add-On for Psono](https://splunkbase.splunk.com/app/4456/)


